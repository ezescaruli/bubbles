//
//  BubblesViewController.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/20/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "Bubble.h"
#import "BubblesViewController.h"
#import "UIView+Frame.h"
#import "Destination.h"
#import "ConfigurationViewController.h"
#import "Configuration.h"
#import "BubbleDetailViewController.h"
#import "CoreDataManager.h"
#import "UIImage+Storage.h"

#define BUBBLE_OFFSET       20
#define VELOCITY_THRESHOLD  150

typedef enum {
    BubblesViewControllerStateInitial   = 0,
    BubblesViewControllerStateMiddle    = 1,
    BubblesViewControllerStateEnd       = 2
} BubblesViewControllerState;

@interface BubblesViewController ()

@property (nonatomic, weak) IBOutlet UILabel *infoLabel;
@property (nonatomic, weak) IBOutlet UILabel *tapAgainLabel;
@property (nonatomic, weak) IBOutlet UIView *bubblesView;

@property (nonatomic, strong) NSArray *imageNames;
@property (nonatomic, strong) NSMutableArray *bubbles; // BubbleView
@property (nonatomic, strong) NSTimer *killerTimer;
@property (nonatomic, strong) UIAlertView *resetAlertView;
@property (nonatomic, assign) BOOL loadingSavedBubbles;
@property (nonatomic, assign) UIInterfaceOrientation currentOrientation;

- (void)showConfigurationViewController;
- (void)addBubbleView:(BubbleView *)bubbleView center:(CGPoint)center;
- (void)handleSingleTap:(UITapGestureRecognizer *)gestureRecognizer;
- (void)handleDoubleTap:(UITapGestureRecognizer *)gestureRecognizer;
- (void)handleBubblePan:(UIPanGestureRecognizer *)gestureRecognizer;
- (void)setBoundForBubbleView:(BubbleView *)bubbleView;
- (BubbleViewBlock)blockForBubbleView:(BubbleView *)bubbleView withBound:(BubbleViewBound)bound;
- (void)keepOnMovingBubbleView:(BubbleView *)bubbleView withVelocity:(CGPoint)velocity;
- (Destination *)destinationForBubbleView:(BubbleView *)bubbleView withVelocity:(CGPoint)velocity;
- (void)reset;
- (void)killAllBubbles;
- (void)setState:(BubblesViewControllerState)state;
- (BOOL)existsSubviewAtLocation:(CGPoint)location;
- (CGFloat)velocityModulus:(CGPoint)velocity;
- (void)turnOffBubblesRemoveMode;
- (UIImage *)randomBubbleImage;
- (void)loadSavedBubbles;
- (Bubble *)newBubbleWithImage:(UIImage *)image location:(CGPoint)location;
- (void)willRotateStoppingBubblesTranslation:(BOOL)stopTranslation;
- (void)didRotate;
- (BOOL)orientation:(UIInterfaceOrientation)firstOrientation isDifferentFromOrientation:(UIInterfaceOrientation)secondOrientation;

@end

@implementation BubblesViewController

- (NSString *)nibName {
    return NSStringFromClass([BubblesViewController class]);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Info labels.
    self.infoLabel.shadowColor = [UIColor whiteColor];
    self.infoLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    self.tapAgainLabel.shadowColor = [UIColor whiteColor];
    self.tapAgainLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    self.tapAgainLabel.alpha = 0.0;
    
    self.imageNames = @[@"angrybird_red.png",
                        @"angrybird_yellow.png",
                        @"angrybird_black.png",
                        @"angrybird_pig.png",
                        @"angrybird_white.png",
                        @"angrybird_blue.png",
                        @"angrybird_lightblue.png",
                        @"angrybird_green.png"];
    
    self.bubbles = [NSMutableArray array];
    
    self.title = @"Bubble Board";
    self.view.backgroundColor = [UIColor underPageBackgroundColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Reset"
                                                                              style:UIBarButtonSystemItemAction
                                                                             target:self
                                                                             action:@selector(reset)];
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    self.resetAlertView = [[UIAlertView alloc] initWithTitle:@"Reset"
                                                     message:@"Are you sure you want to remove all bubbles?"
                                                    delegate:self
                                           cancelButtonTitle:@"No"
                                           otherButtonTitles:@"Yes", nil];
    
    // Single tap gesture recognizing.
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    [singleTapGestureRecognizer addTarget:self action:@selector(handleSingleTap:)];
    [self.bubblesView addGestureRecognizer:singleTapGestureRecognizer];
    
    // Double tap gesture recognizing.
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] init];
    gestureRecognizer.numberOfTapsRequired = 2;
    [gestureRecognizer addTarget:self action:@selector(handleDoubleTap:)];
    [self.bubblesView addGestureRecognizer:gestureRecognizer];
    
    // We register to be notified about orientation changes.
    self.currentOrientation = [[UIDevice currentDevice] orientation];
    
    [self loadSavedBubbles];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // We update each bubble view.
    for (BubbleView *bubble in self.bubbles) {
        [bubble update];
    }
    
    // If orientation has changed, we handle this change.
    UIInterfaceOrientation orientation = self.interfaceOrientation;
    if ([self orientation:self.currentOrientation isDifferentFromOrientation:orientation]) {
        [self willRotateStoppingBubblesTranslation:NO];
    }
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    // If orientation has changed, we handle this change.
    UIInterfaceOrientation orientation = self.interfaceOrientation;
    if ([self orientation:self.currentOrientation isDifferentFromOrientation:orientation]) {
        [self didRotate];
    }
    
    self.currentOrientation = orientation;
}


- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.currentOrientation = [[UIDevice currentDevice] orientation];
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self willRotateStoppingBubblesTranslation:YES];
}


- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self didRotate];
}


- (NSUInteger)supportedInterfaceOrientations {
    if (self.loadingSavedBubbles) {
        return  UIInterfaceOrientationMaskPortrait;
    }
    
    return  UIInterfaceOrientationMaskAllButUpsideDown;
}


# pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        // We use a selector with delay to avoid odd effects that occur when
        // bubbles are being animated and the alert view is disappearing at the
        // same time.
        [self performSelector:@selector(killAllBubbles) withObject:nil afterDelay:0.25];
    }
}


# pragma mark - Private methods


- (void)showConfigurationViewController {
    ConfigurationViewController *configurationViewController = [[ConfigurationViewController alloc] init];
    configurationViewController.bubblesViewController = self;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:configurationViewController];
    
    navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:navigationController animated:YES completion:nil];
}


// Handles single tap in view controller's bubbles view.
- (void)handleSingleTap:(UITapGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint location = [gestureRecognizer locationInView:self.bubblesView];
        
        if (![self existsSubviewAtLocation:location]) {
            [self turnOffBubblesRemoveMode];
        }
    }
}


// Handles double tap in view controller's bubbles view.
- (void)handleDoubleTap:(UITapGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        CGPoint location = [gestureRecognizer locationInView:self.bubblesView];
        
        if (![self existsSubviewAtLocation:location]) {
            UIImage *image = [self randomBubbleImage];
            Bubble *bubble = [self newBubbleWithImage:image location:location];
            BubbleView *bubbleView = [BubbleView bubbleViewWithBubble:bubble
                                                                image:image];
            
            [self addBubbleView:bubbleView center:location];
        }
    }
}


// Adds a bubble centered in the point sent as a parameter.
- (void)addBubbleView:(BubbleView *)bubbleView center:(CGPoint)center {
    if (self.bubbles.count == 0) {
        // This is the first bubble added.
        [self setState:BubblesViewControllerStateMiddle];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        [self setState:BubblesViewControllerStateEnd];
    }
    
    // Add view and set frame.
    [self.bubblesView addSubview:bubbleView];
    [self.bubbles addObject:bubbleView];
    bubbleView.center = center;
    bubbleView.bounds = CGRectMake(0, 0, bubbleView.frameWidth, bubbleView.frameHeight);
    
    // We make bubble appear small at first and then make it grow in an animated way.
    bubbleView.layer.transform = CATransform3DMakeScale(0.1, 0.1, 0.0);
    
    void (^animationBlock)(void) = ^(void) {
        bubbleView.layer.transform = CATransform3DIdentity;
    };
    
    [UIView animateWithDuration:0.25 animations:animationBlock];
    
    // Gesture recognizing.
    UIPanGestureRecognizer *gestureRecognizer = [[UIPanGestureRecognizer alloc] init];
    gestureRecognizer.delegate = bubbleView;
    gestureRecognizer.minimumNumberOfTouches = 1;
    gestureRecognizer.maximumNumberOfTouches = 1;
    [gestureRecognizer addTarget:self action:@selector(handleBubblePan:)];
    [bubbleView addGestureRecognizer:gestureRecognizer];
    
    // Bounds behavior.
    bubbleView.onBoundTopBlock = [self blockForBubbleView:bubbleView withBound:BubbleViewBoundTop];
    bubbleView.onBoundBottomBlock = [self blockForBubbleView:bubbleView withBound:BubbleViewBoundBottom];
    bubbleView.onBoundLeftBlock = [self blockForBubbleView:bubbleView withBound:BubbleViewBoundLeft];
    bubbleView.onBoundRightBlock = [self blockForBubbleView:bubbleView withBound:BubbleViewBoundRight];
    
    // Kill behavior.
    __block BubbleView *_bubbleView = bubbleView;
    
    bubbleView.onKillBlock = ^{
        [self.bubbles removeObject:_bubbleView];
        
        if (self.bubbles.count == 0) {
            [self setState:BubblesViewControllerStateInitial];
            self.navigationItem.rightBarButtonItem.enabled = NO;
        }
        
        [_bubbleView.bubble deleteBubble];
        [[CoreDataManager sharedInstance] saveChanges];
        
        [UIImage deleteFromCacheImageNamed:_bubbleView.bubble.bubbleId];
    };
    
    // Push button action handling.
    bubbleView.onPushButtonTapBlock = ^{
        [self turnOffBubblesRemoveMode];
        
        BubbleDetailViewController *detailViewController = [[BubbleDetailViewController alloc] initWithBubbleView:_bubbleView];
        [self.navigationController pushViewController:detailViewController animated:YES];
    };
}


// Returns the block that a bubble view must run when it reaches the given
// bound.
- (BubbleViewBlock)blockForBubbleView:(BubbleView *)bubbleView withBound:(BubbleViewBound)bound {
    return ^(void) {
        void (^animationBlock)(void) = ^(void) {
            switch (bound) {
                case BubbleViewBoundLeft:
                    bubbleView.centerX = BUBBLE_OFFSET;
                    break;
                    
                case BubbleViewBoundRight:
                    bubbleView.centerX = self.bubblesView.frameWidth - BUBBLE_OFFSET;
                    break;
                    
                case BubbleViewBoundTop:
                    bubbleView.centerY = BUBBLE_OFFSET;
                    break;
                    
                case BubbleViewBoundBottom:
                    bubbleView.centerY = self.bubblesView.frameHeight - BUBBLE_OFFSET;
                    break;
                    
                default:
                    break;
            }
            
            // We update the bubble position.
            bubbleView.bubble.x = @(bubbleView.centerX / self.view.frameWidth);
            bubbleView.bubble.y = @(bubbleView.centerY / self.view.frameHeight);
            [[CoreDataManager sharedInstance] saveChanges];
        };
        
        [UIView animateWithDuration:0.5 animations:animationBlock];
    };
}


// Handles the pan gesture on a bubble view.
- (void)handleBubblePan:(UIPanGestureRecognizer *)gestureRecognizer {
    BubbleView *bubbleView = (BubbleView *) gestureRecognizer.view;
    CGPoint bubbleTouchLocation = [gestureRecognizer locationInView:bubbleView];
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        // I'm starting the movement, so I save a center offset.
        
        bubbleView.centerOffset = CGPointMake(bubbleTouchLocation.x - bubbleView.frameWidth / 2,
                                              bubbleTouchLocation.y - bubbleView.frameHeight / 2);
        
        // I stop the movement to support the case in which the bubble was
        // previously moving.
        [bubbleView stopMovement];
    }
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        // I have finished the movement.
        
        __unsafe_unretained BubbleView *_bubbleView = bubbleView;
        bubbleView.onBoundNoneBlock = ^{
            // If the bubble has not reached a block, we validate if we keep
            // moving it in an animaed way.
            CGPoint velocity = [gestureRecognizer velocityInView:self.bubblesView];
            
            if ([self velocityModulus:velocity] > VELOCITY_THRESHOLD) {
                // We keep on moving the bubble.
                [self keepOnMovingBubbleView:_bubbleView withVelocity:velocity];
            } else {
                // We don't keep on moving the bubble, so we save the point
                // where it stopped moving.
                _bubbleView.bubble.x = @(_bubbleView.centerX / self.view.frameWidth);
                _bubbleView.bubble.y = @(_bubbleView.centerY / self.view.frameHeight);
                
                [[CoreDataManager sharedInstance] saveChanges];
            }
        };
        [bubbleView performBoundBehavior];
        
        return;
    }
    
    // I am moving the bubble.
    
    CGPoint hereTouchLocation = [gestureRecognizer locationInView:self.bubblesView];
    
    bubbleView.center = CGPointMake(hereTouchLocation.x - bubbleView.centerOffset.x,
                                    hereTouchLocation.y - bubbleView.centerOffset.y);
    
    [self setBoundForBubbleView:bubbleView];
}


// Sets the appropiate bound for a bubble view depending on its position.
- (void)setBoundForBubbleView:(BubbleView *)bubbleView {
    if (bubbleView.centerX < BUBBLE_OFFSET) {
        bubbleView.currentBound = BubbleViewBoundLeft;
        return;
    }
    
    if (bubbleView.centerX > self.bubblesView.frameWidth - BUBBLE_OFFSET) {
        bubbleView.currentBound = BubbleViewBoundRight;
        return;
    }
    
    if (bubbleView.centerY < BUBBLE_OFFSET) {
        bubbleView.currentBound = BubbleViewBoundTop;
        return;
    }
    
    if (bubbleView.centerY > self.bubblesView.frameHeight - BUBBLE_OFFSET) {
        bubbleView.currentBound = BubbleViewBoundBottom;
        return;
    }
    
    bubbleView.currentBound = BubbleViewBoundNone;
}


// Moves the bubble to a (horizontal or vertical) bound when the pan gesture
// ends.
- (void)keepOnMovingBubbleView:(BubbleView *)bubbleView withVelocity:(CGPoint)velocity {
    PanningMethod panningMethod = [Configuration sharedInstance].panningMethod;
    Destination *destination = [self destinationForBubbleView:bubbleView withVelocity:velocity];
    
    void (^completionBlock)(void) = ^(void) {
        [self setBoundForBubbleView:bubbleView];
        
        // If I have reached a bound, I perform its bound behaviour.
        if (bubbleView.currentBound != BubbleViewBoundNone) {
            [bubbleView performBoundBehavior];
        }
    };
    
    [bubbleView moveToDestination:destination velocity:velocity panningMethod:panningMethod onCompletion:completionBlock];
}


// Calculates the destination for a bubble which pan gesture has ended.
- (Destination *)destinationForBubbleView:(BubbleView *)bubbleView withVelocity:(CGPoint)velocity {
    if (velocity.x == 0.0 && velocity.y == 0.0) {
        return [[Destination alloc] initWithPoint:bubbleView.center time:0.0];
    }
    
    CGPoint point;
    CGFloat time = 0.0;
    
    // Time that the bubble lasts to reach an horizontal bound.
    CGFloat horizontalBoundTime = 0.0;
    CGFloat boundX;
    
    // Time that the bubble lasts to reach a vertical bound.
    CGFloat verticalBoundTime = 0.0;
    CGFloat boundY;
    
    if (velocity.x != 0.0) {
        horizontalBoundTime = (self.bubblesView.frameWidth - bubbleView.centerX) / velocity.x;
        boundX = self.bubblesView.frameWidth - 1;
        
        if (horizontalBoundTime < 0) {
            horizontalBoundTime = - bubbleView.centerX / velocity.x;
            boundX = 0.0;
        }
    }
    
    if (velocity.y != 0.0) {
        verticalBoundTime = (self.bubblesView.frameHeight - bubbleView.centerY) / velocity.y;
        boundY = self.bubblesView.frameHeight - 1;
        
        if (verticalBoundTime < 0.0) {
            verticalBoundTime = - bubbleView.centerY / velocity.y;
            boundY = 0.0;
        }
    }
    
    if (velocity.x != 0.0 && (velocity.y == 0.0 || horizontalBoundTime < verticalBoundTime)) {
        // I have to go to the horizontal bound.
        time = horizontalBoundTime;
        point.x = boundX;
        point.y = bubbleView.centerY + velocity.y * time;
    } else {
        // I have to go to the vertical bound.
        time = verticalBoundTime;
        point.x = bubbleView.centerX + velocity.x * time;
        point.y = boundY;
    }
    
    return [[Destination alloc] initWithPoint:point time:time];
}


// Handles the reset button action.
- (void)reset {
    [self.resetAlertView show];
}


// Removes all bubbles.
- (void)killAllBubbles {
    self.killerTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                        target:self
                                                      selector:@selector(killRandomBubble)
                                                      userInfo:nil
                                                       repeats:YES];
    [self.killerTimer fire];
}


- (void)killRandomBubble {
    if (self.bubbles.count == 0) {
        [self.killerTimer invalidate];
        [self setState:BubblesViewControllerStateInitial];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        return;
    }
    
    BubbleView *randomBubble = [self.bubbles objectAtIndex:arc4random_uniform(self.bubbles.count)];
    [randomBubble kill];
    [self.bubbles removeObject:randomBubble];
}


// Makes the view controller look different depending on the sent state.
- (void)setState:(BubblesViewControllerState)state {
    void (^animationBlock)(void);
    
    if (state == BubblesViewControllerStateInitial) {
        animationBlock = ^{
            self.infoLabel.alpha = 1.0;
            self.tapAgainLabel.alpha = 0.0;
        };
    }
    
    if (state == BubblesViewControllerStateMiddle) {
        animationBlock = ^{
            self.infoLabel.alpha = 0.0;
            self.tapAgainLabel.alpha = 1.0;
        };
    }
    
    if (state == BubblesViewControllerStateEnd) {
        animationBlock = ^{
            self.infoLabel.alpha = 0.0;
            self.tapAgainLabel.alpha = 0.0;
        };
    }
    
    [UIView animateWithDuration:0.5 animations:animationBlock];
}


// Iterates through current bubbles and returns YES if any frame of them
// contains the sent location.
- (BOOL)existsSubviewAtLocation:(CGPoint)location {
    for (UIView *subview in self.bubblesView.subviews) {
        if (CGRectContainsPoint(subview.frame, location)) {
            return YES;
        }
    }
    
    return NO;
}


// Receives a velocity broken into its horizontal and vertical components, and
// calculates its modulues.
- (CGFloat)velocityModulus:(CGPoint)velocity {
    return sqrtf(velocity.x * velocity.x + velocity.y * velocity.y);
}


// Turns off remove mode for all bubbles.
- (void)turnOffBubblesRemoveMode {
    for (BubbleView *bubble in self.bubbles) {
        if (bubble.removeMode) {
            [bubble showInRemoveMode:NO activatedByUser:YES];
        }
    }
}


// Returns a random image using the property with an array of image names.
- (UIImage *)randomBubbleImage {
    NSString *randomImageName = [self.imageNames objectAtIndex:arc4random_uniform(self.imageNames.count)];
    return [UIImage imageNamed:randomImageName];
}


// Loads bubbles that are saved in Core Data storage and adds them.
- (void)loadSavedBubbles {
    self.loadingSavedBubbles = YES;
    
    NSDictionary *userInfo = @{@"bubbles": [Bubble allBubbles],
                               @"currentIndex": @0};
    
    NSTimer *loaderTimer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                                        target:self
                                                      selector:@selector(loadBubbleWithTimer:)
                                                      userInfo:[NSMutableDictionary dictionaryWithDictionary:userInfo]
                                                       repeats:YES];
    [loaderTimer fire];
}


- (void)loadBubbleWithTimer:(NSTimer *)timer {
    NSMutableDictionary *userInfo = timer.userInfo;
    NSArray *bubbles = [userInfo objectForKey:@"bubbles"];
    NSInteger index = [[userInfo objectForKey:@"currentIndex"] integerValue];
    
    if (index >= bubbles.count) {
        self.loadingSavedBubbles = NO;
        [timer invalidate];
        return;
    }
    
    Bubble *bubble = [bubbles objectAtIndex:index];
    
    UIImage *image = [UIImage imageFromCacheWithName:[bubble bubbleId]];
    BubbleView *bubbleView = [BubbleView bubbleViewWithBubble:bubble
                                                        image:image];
    
    CGPoint center = CGPointMake([bubble.x floatValue] * self.view.frameWidth,
                                 [bubble.y floatValue] * self.view.frameHeight);
    [self addBubbleView:bubbleView center:center];
    
    index++;
    [userInfo setObject:@(index) forKey:@"currentIndex"];
}


// Returns a new bubble with its image saved in the cache folder.
- (Bubble *)newBubbleWithImage:(UIImage *)image location:(CGPoint)location {
    
    // We create the bubble view and set its x and y coordinates.
    Bubble *bubble = [Bubble bubble];
    bubble.name = @"Hello!";
    bubble.x = @(location.x / self.view.frameWidth);
    bubble.y = @(location.y / self.view.frameHeight);
    bubble.imagePath = [bubble bubbleId];
    [image saveInCacheWithName:bubble.imagePath];
    
    [[CoreDataManager sharedInstance] saveChanges];
    
    return bubble;
}


// Called when this view controller's view will be rotated.
- (void)willRotateStoppingBubblesTranslation:(BOOL)stopTranslation {
    for (BubbleView *bubbleView in self.bubbles) {
        [bubbleView willRotateStoppingTranslation:stopTranslation];
    }
}


// Called when this view controller's view did rotated.
- (void)didRotate {
    for (BubbleView *bubbleView in self.bubbles) {
        [bubbleView didRotate];
    }
}


// Determines if two orientations have different rotations.
// This method works on devices, but may fail in simulators.
- (BOOL)orientation:(UIInterfaceOrientation)firstOrientation isDifferentFromOrientation:(UIInterfaceOrientation)secondOrientation {
    if (UIInterfaceOrientationIsLandscape(firstOrientation) &&
        UIInterfaceOrientationIsPortrait(secondOrientation)) {
        return YES;
    }
    
    if (UIInterfaceOrientationIsPortrait(firstOrientation) &&
        UIInterfaceOrientationIsLandscape(secondOrientation)) {
        return YES;
    }
    
    return NO;
}


@end
