//
//  PushButton.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/23/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "PushButton.h"
#import "NSBundle+UIView.h"

@implementation PushButton

+ (PushButton *)pushButton {    
    return (PushButton *) [[NSBundle mainBundle] firstViewFromNibNamed:NSStringFromClass([PushButton class])];
}

@end
