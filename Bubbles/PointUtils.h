//
//  PointUtils.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/2/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PointUtils : NSObject

+ (BOOL)point:(CGPoint)point1 isApproximatelyEqualTo:(CGPoint)point2;
+ (BOOL)point:(CGPoint)point1 isNearToPoint:(CGPoint)point2 usingThreshold:(CGPoint)threshold;

@end
