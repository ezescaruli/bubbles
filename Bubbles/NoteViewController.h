//
//  NoteViewController.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/14/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Note.h"

@interface NoteViewController : UIViewController

@property (nonatomic, strong) Note *note;

- (id)initWithNote:(Note *)note;

@end
