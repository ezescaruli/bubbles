//
//  ConfigurationViewController.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/28/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BubblesViewController.h"

@interface ConfigurationViewController : UIViewController<UITableViewDelegate>

@property (nonatomic, strong) BubblesViewController *bubblesViewController;

@end
