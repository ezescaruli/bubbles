//
//  CoreDataManager.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/9/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <CoreData/CoreData.h>

#import "CoreDataManager.h"

static CoreDataManager *instance = nil;

@interface CoreDataManager()

@property (nonatomic, strong) NSPersistentStoreCoordinator *persistentStoreCoordinatorInstance;
@property (nonatomic, strong) NSManagedObjectModel *managedObjectModelInstance;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContextInstance;

- (NSString *)applicationCacheDirectory;
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSManagedObjectModel *)managedObjectModel;

@end


@implementation CoreDataManager


+ (CoreDataManager *)sharedInstance {
    if (instance == nil) {
        instance = [[CoreDataManager alloc] init];
    }
    
    return instance;
}


- (NSManagedObjectContext *)managedObjectContext {
	
    if (self.managedObjectContextInstance != nil) {
        return self.managedObjectContextInstance;
    }
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        self.managedObjectContextInstance = [[NSManagedObjectContext alloc] init];
        [self.managedObjectContextInstance setPersistentStoreCoordinator:coordinator];
    }
    
    return self.managedObjectContextInstance;
}


- (void)saveChanges {
    NSError *error;
    [[self managedObjectContext] save:&error];
}


- (id)newObjectForEntityClass:(Class)entityClass {
    NSString *className = NSStringFromClass(entityClass);
    NSManagedObjectContext *context = [self managedObjectContext];
    
    return [NSEntityDescription insertNewObjectForEntityForName:className inManagedObjectContext:context];
}


# pragma mark - Private methods


- (NSString *)applicationCacheDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}


- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	
    if (self.persistentStoreCoordinatorInstance != nil) {
        return self.persistentStoreCoordinatorInstance;
    }
	
    NSURL *storeUrl = [NSURL fileURLWithPath: [[self applicationCacheDirectory] stringByAppendingPathComponent:@"Bubbles.sqlite"]];
	
    self.persistentStoreCoordinatorInstance = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
	NSError *error;
    NSPersistentStore *persistentStore = [self.persistentStoreCoordinatorInstance addPersistentStoreWithType:NSSQLiteStoreType
                                                                                               configuration:nil
                                                                                                         URL:storeUrl
                                                                                                     options:nil
                                                                                                       error:&error];
    if (persistentStore == nil) {
        // Handle the error.
    }
	
    return self.persistentStoreCoordinatorInstance;
}


- (NSManagedObjectModel *)managedObjectModel {
	
    if (self.managedObjectModelInstance != nil) {
        return self.managedObjectModelInstance;
    }
    
    self.managedObjectModelInstance = [NSManagedObjectModel mergedModelFromBundles:nil];
    return self.managedObjectModelInstance;
}


@end
