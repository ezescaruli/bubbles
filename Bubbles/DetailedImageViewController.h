//
//  DetailedImageViewController.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/30/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailedImageViewController : UIViewController

- (id)initWithImage:(UIImage *)image;

@end
