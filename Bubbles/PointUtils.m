//
//  PointUtils.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/2/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "PointUtils.h"

#define EPSILON 2.0

@implementation PointUtils

+ (BOOL)point:(CGPoint)point1 isApproximatelyEqualTo:(CGPoint)point2 {
    return ABS(point1.x - point2.x) + ABS(point1.y - point2.y) <= EPSILON;
}

+ (BOOL)point:(CGPoint)point1 isNearToPoint:(CGPoint)point2 usingThreshold:(CGPoint)threshold {
    return ABS(point1.x - point2.x) <= ABS(threshold.x) &&
           ABS(point1.y - point2.y) <= ABS(threshold.y);
}

@end
