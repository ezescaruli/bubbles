//
//  NSString+Utils.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/7/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

- (NSString *)stringByTrimmingBlanks;

@end
