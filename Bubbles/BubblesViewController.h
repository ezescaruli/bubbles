//
//  BubblesViewController.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/20/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BubbleView.h"

@interface BubblesViewController : UIViewController<UIAlertViewDelegate>

@end
