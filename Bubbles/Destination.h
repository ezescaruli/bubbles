//
//  Destination.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/26/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Destination : NSObject

@property (nonatomic) CGPoint point;
@property (nonatomic) CGFloat time;

- (id)initWithPoint:(CGPoint)point time:(CGFloat)time;

@end
