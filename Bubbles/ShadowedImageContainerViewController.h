//
//  ShadowedImageContainerViewController.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/29/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShadowedImageContainerViewController : UIViewController

@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong) void (^onTapBlock)(void);
@property (nonatomic, strong) void (^onEditTapBlock)(void);

- (id)initWithImage:(UIImage *)image;

@end
