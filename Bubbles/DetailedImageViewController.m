//
//  DetailedImageViewController.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/30/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "DetailedImageViewController.h"

@interface DetailedImageViewController ()

@property (nonatomic, weak) IBOutlet UIView *imageContainerView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@property (nonatomic, strong) UIImage *image;

- (void)doneAction;

@end


@implementation DetailedImageViewController


- (id)initWithImage:(UIImage *)image {
    self = [super init];
    
    if (self != nil) {
        self.image = image;
    }
    
    return self;
}


- (NSString *)nibName {
    return NSStringFromClass([DetailedImageViewController class]);
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor underPageBackgroundColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                              style:UIBarButtonSystemItemDone
                                                                             target:self
                                                                             action:@selector(doneAction)];
    
    self.imageView.image = self.image;
    
    // Container view layer.
    self.imageContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.imageContainerView.layer.shadowOpacity = 0.5;
    self.imageContainerView.layer.shadowOffset = CGSizeMake(6.0, 6.0);
    self.imageContainerView.layer.shadowRadius = 2.0;
    self.imageContainerView.layer.masksToBounds = NO;
    
    // Single tap gesture recognizing.
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    [singleTapGestureRecognizer addTarget:self action:@selector(doneAction)];
    [self.view addGestureRecognizer:singleTapGestureRecognizer];
}


# pragma mark - Private methods


// Method performed when 'Done' button is tapped.
- (void)doneAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
