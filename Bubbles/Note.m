//
//  Note.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/14/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "Note.h"
#import "Bubble.h"
#import "CoreDataManager.h"


@implementation Note

@dynamic text;
@dynamic bubble;


+ (Note *)note {
    Note *note = [[CoreDataManager sharedInstance] newObjectForEntityClass:[Note class]];
    [[CoreDataManager sharedInstance] saveChanges];
    
    return note;
}


@end
