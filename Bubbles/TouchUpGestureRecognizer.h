//
//  TouchUpGestureRecognizer.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/13/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchUpGestureRecognizer : UIGestureRecognizer

@end
