//
//  Bubble.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/7/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <CoreData/CoreData.h>

#import "Bubble.h"
#import "CoreDataManager.h"

@implementation Bubble

@dynamic creationDate;
@dynamic name;
@dynamic imagePath;
@dynamic x;
@dynamic y;
@dynamic note;


+ (Bubble *)bubble {
    Bubble *bubble = [[CoreDataManager sharedInstance] newObjectForEntityClass:[Bubble class]];
    bubble.creationDate = [NSDate date];
    [[CoreDataManager sharedInstance] saveChanges];
    
    return bubble;
}


+ (NSArray *)allBubbles {
    NSManagedObjectContext *context = [[CoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bubble" inManagedObjectContext:context];
    request.entity = entity;
    
    NSError *error;
    return [context executeFetchRequest:request error:&error];
}


+ (void)deleteAllBubbles {
    NSManagedObjectContext *context = [[CoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Bubble" inManagedObjectContext:context];
    request.entity = entity;
    request.includesPropertyValues = NO; // Only fetch IDs
    
    NSError *error;
    NSArray *result = [context executeFetchRequest:request error:&error];
    
    for (Bubble *bubble in result) {
        [context deleteObject:bubble];
    }
    
    [[CoreDataManager sharedInstance] saveChanges];
}


- (void)deleteBubble {
    NSManagedObjectContext *context = [[CoreDataManager sharedInstance] managedObjectContext];
    [context deleteObject:self];
    [[CoreDataManager sharedInstance] saveChanges];
}


- (NSString *)bubbleId {
    return [[[self objectID] URIRepresentation] lastPathComponent];
}


@end
