//
//  Configuration.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/28/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BubbleView.h"

@interface Configuration : NSObject

@property (nonatomic) PanningMethod panningMethod;
@property (nonatomic) NSInteger timerInterval; // (ms)
@property (nonatomic) CGFloat friction; // (px/ms)

+ (Configuration *)sharedInstance;

- (NSArray *)panningMethods; // NSNumber

@end
