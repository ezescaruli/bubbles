//
//  BubbleDetailViewController.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/23/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BubbleView.h"

@interface BubbleDetailViewController : UIViewController<UIActionSheetDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate>

- (id)initWithBubbleView:(BubbleView *)bubbleView;

@end
