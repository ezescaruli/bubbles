//
//  AlertUtils.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/9/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertUtils : NSObject

+ (void)showDefaultAlertViewWithTitle:(NSString *)title message:(NSString *)message;

@end
