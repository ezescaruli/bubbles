//
//  NotePreviewView.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/14/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "NotePreviewView.h"

@interface NotePreviewView()

@property (nonatomic, weak) IBOutlet UILabel *label;
@property (nonatomic, weak) IBOutlet UIButton *button;

@property (nonatomic) BOOL initializedWithCoder;

- (IBAction)buttonAction:(id)sender;

- (void)initialize;

@end


@implementation NotePreviewView


- (id)init {
    self = [super init];
    
    if (self != nil) {
        [self initialize];
    }
    
    return self;
}


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self != nil) {
        self.initializedWithCoder = YES;
        
        NSString *className = NSStringFromClass([NotePreviewView class]);
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:className owner:self options:nil];
        UIView *mainView = [subviewArray objectAtIndex:0];
        [self addSubview:mainView];
    }
    
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    [self initialize];
}


- (void)setNote:(Note *)note {
    if (_note != note) {
        _note = note;
    }
    
    self.label.text = note.text;
}


# pragma mark - Private methods


- (void)initialize {
    UIView *view;
    if (self.initializedWithCoder) {
        self.backgroundColor = [UIColor clearColor];
        view = [self.subviews objectAtIndex:0];
    } else {
        view = self;
    }
    
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    view.layer.borderWidth = 1;
    view.layer.cornerRadius = 5;
}


- (IBAction)buttonAction:(id)sender {
    self.buttonActionBlock();
}


@end
