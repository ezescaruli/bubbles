//
//  NotePreviewView.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/14/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Note.h"

@interface NotePreviewView : UIView

@property (nonatomic, strong) Note *note;
@property (nonatomic, strong) void (^buttonActionBlock)(void);

@end
