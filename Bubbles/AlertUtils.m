//
//  AlertUtils.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/9/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "AlertUtils.h"

@implementation AlertUtils

+ (void)showDefaultAlertViewWithTitle:(NSString *)title message:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
    [alertView show];
}

@end
