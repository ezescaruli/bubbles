//
//  ConfigurationViewController.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/28/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "ConfigurationViewController.h"
#import "ConfigurationTableDataSource.h"
#import "Configuration.h"

@interface ConfigurationViewController ()

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ConfigurationTableDataSource *configurationDataSource;

- (void)showBubblesViewController;

@end

@implementation ConfigurationViewController

- (NSString *)nibName {
    return NSStringFromClass([ConfigurationViewController class]);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"Configuration";
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Return"
                                                                             style:UIBarButtonSystemItemAction
                                                                            target:self
                                                                            action:@selector(showBubblesViewController)];
    
    self.configurationDataSource = [[ConfigurationTableDataSource alloc] init];
    self.tableView.dataSource = self.configurationDataSource;
    self.tableView.delegate = self;
}

- (void)showBubblesViewController {
    [self.configurationDataSource.timerIntervalTextField resignFirstResponder];
    [self.configurationDataSource.frictionTextField resignFirstResponder];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.bubblesViewController];
    navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self presentViewController:navigationController animated:YES completion:nil];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        // Panning method section.
        PanningMethod panningMethod = indexPath.row;
        
        if ([Configuration sharedInstance].panningMethod != panningMethod) {
            [Configuration sharedInstance].panningMethod = panningMethod;
            
            [tableView beginUpdates];
            
            if (panningMethod == PanningMethodTimer) {
                [tableView insertSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
            } else {
                [tableView deleteSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
            }
            
            [tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
            
            [tableView endUpdates];
        }
    }

    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
