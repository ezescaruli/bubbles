//
//  BubbleView.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/20/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Destination.h"
#import "Bubble.h"

#define BUBBLE_VIEW_NAME_MAX_LENGTH 15

typedef void (^BubbleViewBlock)(void);

typedef enum {
    BubbleViewBoundNone     = 0,
    BubbleViewBoundTop      = 1 << 0,
    BubbleViewBoundLeft     = 1 << 1,
    BubbleViewBoundBottom   = 1 << 2,
    BubbleViewBoundRight    = 1 << 3
} BubbleViewBound;

typedef enum {
    PanningMethodAnimation  = 0,
    PanningMethodTimer      = 1
} PanningMethod;

@interface BubbleView : UIView<UIGestureRecognizerDelegate>

@property (nonatomic, weak) IBOutlet UIView *containerView;

@property (nonatomic, strong) Bubble *bubble;
@property (nonatomic, strong) UIImage *image;

@property (nonatomic, strong) BubbleViewBlock onBoundTopBlock;
@property (nonatomic, strong) BubbleViewBlock onBoundLeftBlock;
@property (nonatomic, strong) BubbleViewBlock onBoundBottomBlock;
@property (nonatomic, strong) BubbleViewBlock onBoundRightBlock;
@property (nonatomic, strong) BubbleViewBlock onBoundNoneBlock;

@property (nonatomic, strong) BubbleViewBlock onKillBlock;

@property (nonatomic, strong) BubbleViewBlock onPushButtonTapBlock;

@property (nonatomic) CGPoint centerOffset;
@property (nonatomic) BubbleViewBound currentBound;
@property (nonatomic) BOOL removeMode;
@property (nonatomic, assign) BOOL hasToBeUpdated;

+ (BubbleView *)bubbleViewWithBubble:(Bubble *)bubble image:(UIImage *)image;

// Executes the appropiate block depending on the bubble's current bound.
// After this method call, the bubble's bound is BubbleViewBoundNone.
- (void)performBoundBehavior;

// Moves the bubble to a given destination.
- (void)moveToDestination:(Destination *)destination
                 velocity:(CGPoint)velocity
            panningMethod:(PanningMethod)panningMethod
             onCompletion:(void (^)(void))completionBlock;

// Stops the movement of the bubble (only works with timer panning method).
- (void)stopMovement;

// Makes the ball go out of its superview with an animated style.
- (void)kill;

// Makes the bubbles look as being able to be removed.
- (void)showInRemoveMode:(BOOL)show activatedByUser:(BOOL)activatedByUser;

// These methods are expected to be called from a view controller that handles
// rotation.
- (void)willRotateStoppingTranslation:(BOOL)stopTranslation;
- (void)didRotate;

// Updates bubble's attributes (for example, its bubble name within the popup).
// Useful when some of its attributes has been edited.
- (void)update;

@end
