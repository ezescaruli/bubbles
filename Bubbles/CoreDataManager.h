//
//  CoreDataManager.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/9/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataManager : NSObject

+ (CoreDataManager *)sharedInstance;

// Returns the managed object context, which is initialized just one time.
- (NSManagedObjectContext *)managedObjectContext;

// Persists all the changes done in the context into disk.
- (void)saveChanges;

// Creates an empty, new object of the given class (which has to inherit from
// NSManagedObject). This object is initialized in the current context, but
// not persisted in disk.
- (id)newObjectForEntityClass:(Class)entityClass;

@end
