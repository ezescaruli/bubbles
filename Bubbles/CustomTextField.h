//
//  CustomTextField.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/9/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTextField : UITextField

@end
