//
//  BubbleDetailViewController.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/23/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "BubbleDetailViewController.h"
#import "ShadowedImageContainerViewController.h"
#import "DetailedImageViewController.h"
#import "AlertUtils.h"
#import "UIView+Frame.h"
#import "NSString+Utils.h"
#import "CoreDataManager.h"
#import "UIImage+Storage.h"
#import "AddView.h"
#import "NoteViewController.h"
#import "NotePreviewView.h"

typedef enum {
    ImageSelectOptionExisting = 0,
    ImageSelectOptionTakePhoto = 1,
    ImageSelectOptionNone = 2
} ImageSelectOption;

@interface BubbleDetailViewController()

@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UITextField *nameTextField;
@property (nonatomic, weak) IBOutlet AddView *addView;
@property (nonatomic, weak) IBOutlet NotePreviewView *notePreviewView;
@property (nonatomic, weak) IBOutlet UILabel *creationDateLabel;

@property (nonatomic, strong) BubbleView *bubbleView;
@property (nonatomic, strong) ShadowedImageContainerViewController *imageContainerViewController;

- (BOOL)validateBubbleName;
- (void)setupImageContainerViewController;
- (void)setupAddNoteView;
- (void)setupNotePreviewView;
- (void)setupCreationDateLabel;
- (void)arrangeNoteViews;

@end

@implementation BubbleDetailViewController


- (NSString *)nibName {
    return NSStringFromClass([BubbleDetailViewController class]);
}


- (id)initWithBubbleView:(BubbleView *)bubbleView {
    self = [super init];
    
    if (self != nil) {
        self.bubbleView = bubbleView;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Bubble detail";
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"tableViewBackground.png"]];
    
    // Edit button.
    self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    // Image container.
    [self setupImageContainerViewController];
    
    // Name.
    self.nameLabel.text = self.bubbleView.bubble.name;
    self.nameLabel.shadowColor = [UIColor whiteColor];
    self.nameLabel.shadowOffset = CGSizeMake(1.0, 1.0);
    
    // Add note view.
    [self setupAddNoteView];
    
    // Note preview view.
    [self setupNotePreviewView];
    
    // Creation date.
    [self setupCreationDateLabel];
    
    // We set the editing mode as OFF at first.
    [self setEditing:NO animated:NO];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self arrangeNoteViews];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.nameTextField resignFirstResponder];
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    [self.imageContainerViewController setEditing:editing animated:animated];
    
    self.nameLabel.hidden = editing;
    self.nameTextField.hidden = !editing;
    
    if (editing) {
        self.nameTextField.text = self.bubbleView.bubble.name;
    } else {
        // We validate name.
        if (self.nameTextField.isFirstResponder) {
            [self.nameTextField resignFirstResponder];
        } else {
            [self validateBubbleName];
        }
    }
}


# pragma mark - UIActionSheetDelegate


- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {    
    if (buttonIndex != actionSheet.numberOfButtons - 1) {
        // We have not selected 'Cancel' option.
        
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.delegate = self;
        
        if (buttonIndex == ImageSelectOptionExisting) {
            // Take photo from library.
            imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
        
        if (buttonIndex == ImageSelectOptionTakePhoto) {
            // Take photo with camera.
            imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        }
        
        [self presentViewController:imagePickerController animated:YES completion:nil];
    }
}


# pragma mark - UIImagePickerControllerDelegate


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *selectedImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    self.bubbleView.image = selectedImage;
    self.imageContainerViewController.image = selectedImage;
    
    NSString *imageName = self.bubbleView.bubble.bubbleId;
    [self.bubbleView.image saveInCacheWithName:imageName];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


# pragma mark - UITextFieldDelegate


- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (![self validateBubbleName]) {
        return;
    }
    
    NSString *newName = [self.nameTextField.text stringByTrimmingBlanks];
    
    if (![self.bubbleView.bubble.name isEqualToString:newName]) {
        // We have changed bubble view's name.
        self.bubbleView.bubble.name = newName;
        self.bubbleView.hasToBeUpdated = YES;
        self.nameLabel.text = newName;
        
        [[CoreDataManager sharedInstance] saveChanges];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


# pragma mark - Private methods


- (BOOL)validateBubbleName {
    NSString *name = [self.nameTextField.text stringByTrimmingBlanks];
    
    if (name.length == 0) {
        NSString *message = @"Name cannot be empty.";
        [AlertUtils showDefaultAlertViewWithTitle:@"Empty name" message:message];
        return NO;
    }
    
    if (name.length > BUBBLE_VIEW_NAME_MAX_LENGTH) {
        NSString *message = [NSString stringWithFormat:@"Name cannot be longer than %d characters.",
                             BUBBLE_VIEW_NAME_MAX_LENGTH];
        
        [AlertUtils showDefaultAlertViewWithTitle:@"Name too long" message:message];
        return NO;
    }
    
    return YES;
}


- (void)setupImageContainerViewController {
    self.imageContainerViewController = [[ShadowedImageContainerViewController alloc] initWithImage:self.bubbleView.image];
    self.imageContainerViewController.view.frame = CGRectMake(12.0,
                                                              12.0,
                                                              self.imageContainerViewController.view.frameWidth,
                                                              self.imageContainerViewController.view.frameHeight);
    [self addChildViewController:self.imageContainerViewController];
    [self.view addSubview:self.imageContainerViewController.view];
    
    __unsafe_unretained BubbleDetailViewController *safeMe = self;
    
    self.imageContainerViewController.onTapBlock = ^{
        DetailedImageViewController *imageViewController = [[DetailedImageViewController alloc] initWithImage:safeMe.bubbleView.image];
        
        [safeMe presentViewController:imageViewController animated:YES completion:nil];
    };
    
    self.imageContainerViewController.onEditTapBlock = ^{
        NSMutableArray *buttonTitles = [NSMutableArray arrayWithObject:@"Choose existing image"];
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            [buttonTitles addObject:@"Take photo"];
        }
        
        UIActionSheet *actionSheet;
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Edit image"
                                                      delegate:safeMe
                                             cancelButtonTitle:@"Cancel"
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:@"Choose existing image", @"Take photo", nil];
        } else {
            actionSheet = [[UIActionSheet alloc] initWithTitle:@"Edit image"
                                                      delegate:safeMe
                                             cancelButtonTitle:@"Cancel"
                                        destructiveButtonTitle:nil
                                             otherButtonTitles:@"Choose existing image", nil];
        }
        
        [actionSheet showInView:safeMe.view];
    };
}


- (void)setupAddNoteView {
    self.addView.leftImage = [UIImage imageNamed:@"note.png"];
    self.addView.title = @"Attach a note";
    
    __unsafe_unretained BubbleDetailViewController *safeMe = self;
    self.addView.buttonActionBlock = ^{
        // We show a modal view controller to write the note.
        Note *note = self.bubbleView.bubble.note;
        
        if (note == nil) {
            note = [Note note];
            self.bubbleView.bubble.note = note;
        }
        
        NoteViewController *noteViewController = [[NoteViewController alloc] initWithNote:note];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:noteViewController];
        
        [safeMe presentViewController:navigationController animated:YES completion:nil];
    };
}


- (void)setupNotePreviewView {
    self.notePreviewView.note = self.bubbleView.bubble.note;
    
    __unsafe_unretained BubbleDetailViewController *safeMe = self;
    self.notePreviewView.buttonActionBlock = ^{
        // We show a modal view controller to write the note.
        Note *note = self.bubbleView.bubble.note;
        NoteViewController *noteViewController = [[NoteViewController alloc] initWithNote:note];
        [safeMe.navigationController pushViewController:noteViewController animated:YES];
    };
}


- (void)setupCreationDateLabel {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    formatter.dateFormat = @"d MMM yyyy";
    NSString *dateText = [formatter stringFromDate:self.bubbleView.bubble.creationDate];
    
    formatter.dateFormat = @"HH:mm";
    NSString *timeText = [formatter stringFromDate:self.bubbleView.bubble.creationDate];
    
    self.creationDateLabel.text = [NSString stringWithFormat:@"Created on %@ at %@", dateText, timeText];
}


- (void)arrangeNoteViews {
    Note *note = self.bubbleView.bubble.note;
    BOOL existsNoteText = note.text != nil && note.text.length > 0;
    
    if (existsNoteText && !self.addView.hidden) {
        self.notePreviewView.hidden = NO;
        self.notePreviewView.frameY = self.addView.frameY;
        self.addView.hidden = YES;
    }
    
    if (!existsNoteText && !self.notePreviewView.hidden) {
        self.addView.hidden = NO;
        self.notePreviewView.hidden = YES;
    }
    
    self.notePreviewView.note = note;
}


@end
