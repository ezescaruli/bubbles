//
//  Note.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/14/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Bubble;

@interface Note : NSManagedObject

@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) Bubble *bubble;

// Creates a new note and its entity in Core Data context and persists it.
+ (Note *)note;

@end
