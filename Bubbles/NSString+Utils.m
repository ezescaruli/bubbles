//
//  NSString+Utils.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/7/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "NSString+Utils.h"

@implementation NSString (Utils)

- (NSString *)stringByTrimmingBlanks {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
