//
//  AppDelegate.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/20/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
