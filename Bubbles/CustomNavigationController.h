//
//  CustomNavigationController.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/20/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationController : UINavigationController

@end
