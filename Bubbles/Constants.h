//
//  Constants.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/5/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#ifndef Bubbles_Constants_h
#define Bubbles_Constants_h

// Rectangles

#define kDefaultCellTextFieldRect   CGRectMake(180, 10, 105, 30)

// Colors

#define kAppleLightBlueColor        [UIColor colorWithRed:0.18 green:0.29 blue:0.55 alpha:1.0]

#endif
