//
//  BubbleView.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/20/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "BubbleView.h"

#import <QuartzCore/QuartzCore.h>

#import "UIView+Frame.h"
#import "PointUtils.h"
#import "Configuration.h"
#import "TouchDownGestureRecognizer.h"
#import "TouchUpGestureRecognizer.h"
#import "QBPopupMenu.h"
#import "PushButton.h"
#import "NSBundle+UIView.h"

#define DESTINATION_RECT_SIZE   20.0

@interface BubbleView()

@property (nonatomic, weak) IBOutlet UIView *shadowView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIButton *removeButton;

- (IBAction)removeButtonAction:(id)sender;

@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, assign) CGFloat xFriction;
@property (nonatomic, assign) CGFloat yFriction;
@property (nonatomic, assign) CGRect destinationRect;
@property (nonatomic, assign) NSInteger rotationDirection;
@property (nonatomic, strong) QBPopupMenu *popupMenu;
@property (nonatomic, assign) BOOL hasToRotatePopupMenu;

- (void)initialize;
- (void)handleTouchDown:(TouchDownGestureRecognizer *)gestureRecognizer;
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;
- (void)calculateFrictionForVelocity:(CGPoint)velocity;
- (CGPoint)slowDownTranslation:(CGPoint)translation;
- (void)calculateDestinationRect:(CGPoint)destinationPoint;
- (void)setUpPopupMenu;
- (void)showPopupMenu;
- (void)stopTranslationAnimation;
- (void)pushButtonAction;

@end

@implementation BubbleView

+ (BubbleView *)bubbleViewWithBubble:(Bubble *)bubble image:(UIImage *)image {
    BubbleView *ret = (BubbleView *) [[NSBundle mainBundle] firstViewFromNibNamed:NSStringFromClass([BubbleView class])];
    
    if (ret != nil) {
        ret.bubble = bubble;
        ret.image = image;
        [ret setUpPopupMenu];
    }
    
    return ret;
}

- (id)init {
    self = [super init];
    
    if (self != nil) {
        [self initialize];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initialize];
}

- (void)setImage:(UIImage *)anImage {
    if (_image != anImage) {
        _image = anImage;
        self.imageView.image = _image;
    }
}

- (void)performBoundBehavior {
    switch (self.currentBound) {
        case BubbleViewBoundLeft:
            self.onBoundLeftBlock();
            break;
            
        case BubbleViewBoundRight:
            self.onBoundRightBlock();
            break;
            
        case BubbleViewBoundTop:
            self.onBoundTopBlock();
            break;
            
        case BubbleViewBoundBottom:
            self.onBoundBottomBlock();
            break;
            
        default:
            self.onBoundNoneBlock();
    }
    
    self.currentBound = BubbleViewBoundNone;
}

- (void)moveToDestination:(Destination *)destination
                 velocity:(CGPoint)velocity
            panningMethod:(PanningMethod)panningMethod
             onCompletion:(void (^)(void))completionBlock {
    
    if (panningMethod == PanningMethodAnimation) {
        NSDictionary *dictionary = @{@"destination": destination,
                                     @"completionBlock": completionBlock};
        
        if (destination.time != 0.0) {
            [self performSelector:@selector(keepOnMovingBubbleViewWithAnimationSelector:) withObject:dictionary afterDelay:0.01];
        }
    }
    
    if (panningMethod == PanningMethodTimer) {
        CGFloat timerIntervalInSeconds = [Configuration sharedInstance].timerInterval / 1000.0;
        
        CGPoint translation = CGPointMake(timerIntervalInSeconds * velocity.x,
                                          timerIntervalInSeconds * velocity.y);
        
        [self calculateDestinationRect:destination.point];
        [self calculateFrictionForVelocity:velocity];
        
        NSDictionary *dictionary = @{@"translation": [NSValue valueWithCGPoint:translation],
                                     @"completionBlock": completionBlock};
        
        NSMutableDictionary *mutableDictionary = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        
        self.timer = [NSTimer scheduledTimerWithTimeInterval:timerIntervalInSeconds
                                                      target:self
                                                    selector:@selector(keepOnMovingBubbleViewWithTimerSelector:)
                                                    userInfo:mutableDictionary
                                                     repeats:YES];
        [self.timer fire];
    }
}

- (void)keepOnMovingBubbleViewWithAnimationSelector:(NSDictionary *)dictionary {
    Destination *destination = [dictionary objectForKey:@"destination"];
    void (^completionBlock)(void) = [dictionary objectForKey:@"completionBlock"];
    
    void (^animationBlock)(void) = ^(void) {
        self.center = destination.point;
    };
    
    void (^animationCompletionBlock)(BOOL) = ^(BOOL finished) {
        completionBlock();
    };
    
    [UIView animateWithDuration:destination.time animations:animationBlock completion:animationCompletionBlock];
}

- (void)keepOnMovingBubbleViewWithTimerSelector:(NSTimer *)timer {
    NSMutableDictionary *dictionary = timer.userInfo;
    CGPoint translation = [[dictionary objectForKey:@"translation"] CGPointValue];
    void (^completionBlock)(void) = [dictionary objectForKey:@"completionBlock"];

    BOOL reachedDestination = CGRectContainsPoint(self.destinationRect, self.center);
    BOOL isStopped = translation.x == 0.0 || translation.y == 0.0;
    
    if (reachedDestination || isStopped) {
        [self stopMovement];
        completionBlock();
        return;
    }
    
    self.centerX += translation.x;
    self.centerY += translation.y;
    
    translation = [self slowDownTranslation:translation];
    [dictionary setObject:[NSValue valueWithCGPoint:translation] forKey:@"translation"];
}

- (void)stopMovement {
    [self.timer invalidate];
}

- (void)kill {
    [self stopTranslationAnimation];
    
    // If we are in remove mode, we finish it.
    if (self.removeMode) {
        [self showInRemoveMode:NO activatedByUser:NO];
    }
    
    // We dismiss the popup.
    [self.popupMenu dismiss];
    
    void (^rotateBlock)(void) = ^{
        // I multiply M_PI by 0.99 because, if I use M_PI directly, the
        // rotation is done always in the same direction.
        self.layer.affineTransform = CGAffineTransformMakeRotation(self.rotationDirection * M_PI * 0.99);
    };
    
    void (^completionBlock)(BOOL) = ^(BOOL finished) {
        void (^fallDownBlock)(void) = ^{
            self.frameY = self.superview.frameHeight;
        };
        
        void (^removeBlock)(BOOL) = ^(BOOL finished) {
            [self removeFromSuperview];
        };
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:fallDownBlock
                         completion:removeBlock];
    };
    
    [UIView animateWithDuration:0.5 animations:rotateBlock completion:completionBlock];
    
    self.onKillBlock();
}


- (void)showInRemoveMode:(BOOL)show activatedByUser:(BOOL)activatedByUser {
    self.removeMode = show;
    self.removeButton.hidden = !show;
    
    if (show) {
        CGFloat rotationAngle = 0.5 * M_PI_4;
        CGFloat firstAnimationDuration = activatedByUser ? 0.25 : 0.5;
        
        // We use this restore block to put the bubble in its original
        // position when the remove mode is deactivated.
        void (^restoreBlock)(void) = ^{
            self.imageView.layer.affineTransform = CGAffineTransformIdentity;
        };
        
        void (^firstAnimationBlock)(void) = ^{
            self.rotationDirection = -1;
            self.imageView.layer.affineTransform = CGAffineTransformMakeRotation(-rotationAngle);
        };
        
        void (^firstCompletionBlock)(BOOL) = ^(BOOL finished) {
            if (self.removeMode) {
                // If we are in remove mode yet, we keep on rotating.
                
                void (^secondAnimationBlock)(void) = ^{
                    self.rotationDirection = 1;
                    self.imageView.layer.affineTransform = CGAffineTransformMakeRotation(rotationAngle);
                };
                
                void (^secondCompletionBlock)(BOOL) = ^(BOOL finished) {
                    if (self.removeMode) {
                        // If we are in remove mode yet, we keep on rotating.
                        [self showInRemoveMode:YES activatedByUser:NO];
                    } else {
                        // If we are not anymore in remove mode, we restore the bubble
                        // to its original position.
                        [UIView animateWithDuration:0.5 animations:restoreBlock];
                    }
                };
                
                [UIView animateWithDuration:0.5 animations:secondAnimationBlock completion:secondCompletionBlock];
            } else {
                // If we are not anymore in remove mode, we restore the bubble
                // to its original position.
                [UIView animateWithDuration:0.5 animations:restoreBlock];
            }
        };
        
        [UIView animateWithDuration:firstAnimationDuration animations:firstAnimationBlock completion:firstCompletionBlock];
    }
}


- (void)willRotateStoppingTranslation:(BOOL)stopTranslation {
    if (self.popupMenu.visible) {
        self.hasToRotatePopupMenu = YES;
        [self.popupMenu dismiss];
    } else {
        self.hasToRotatePopupMenu = NO;
    }
    
    if (stopTranslation) {
        [self stopTranslationAnimation];
    }
}


- (void)didRotate {
    if (self.hasToRotatePopupMenu) {
        [self showPopupMenu];
    }
}


- (void)update {
    if (self.hasToBeUpdated) {
        // Popup update.
        
        BOOL popupIsVisible = self.popupMenu.visible;
        [self.popupMenu dismiss];
        [self setUpPopupMenu];
        
        if (popupIsVisible) {
            [self showPopupMenu];
        }
    
        self.hasToBeUpdated = NO;
    }
}


# pragma mark - UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

# pragma mark - Private methods

- (void)initialize {
    self.currentBound = BubbleViewBoundNone;
    self.removeMode = NO;
    self.rotationDirection = 1;
    self.hasToRotatePopupMenu = NO;
    
    // Friction.
    self.xFriction = 0.0;
    self.yFriction = 0.0;
    
    // Image border settings.
    self.imageView.layer.borderWidth = 2.0;
    self.imageView.layer.borderColor = [UIColor grayColor].CGColor;
    self.imageView.layer.cornerRadius = self.imageView.frame.size.width / 2.0;
    self.imageView.layer.masksToBounds = YES;
    
    // Shadow settings.
    
    self.layer.masksToBounds = NO;
    CALayer *shadowLayer = self.shadowView.layer;
    
    // We make the shadow layer be a bit 'outside' in order to perform an
    // outline effect.
    shadowLayer.position = CGPointMake(self.imageView.center.x - 1.0,
                                       self.imageView.center.y - 1.0);
    shadowLayer.bounds = CGRectMake(0,
                                    0,
                                    self.imageView.bounds.size.width + 2.0,
                                    self.imageView.bounds.size.height + 2.0);
    
    shadowLayer.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0].CGColor;
    shadowLayer.shadowOpacity = 0.75;
    shadowLayer.shadowOffset = CGSizeMake(1.0, 1.0);
    shadowLayer.shadowColor = [UIColor blackColor].CGColor;
    shadowLayer.masksToBounds = NO;
    
    // We use a rounded path for the shadow.
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRoundedRect:shadowLayer.bounds
                                                          cornerRadius:(shadowLayer.bounds.size.width * 0.5)];
    shadowLayer.shadowPath = shadowPath.CGPath;
    
    // Touch down gesture recognizing.
    TouchDownGestureRecognizer *touchDownGestureRecognizer = [[TouchDownGestureRecognizer alloc] init];
    touchDownGestureRecognizer.delegate = self;
    [touchDownGestureRecognizer addTarget:self action:@selector(handleTouchDown:)];
    [self.containerView addGestureRecognizer:touchDownGestureRecognizer];
    
    // Double tap gesture recognizing.
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    tapGestureRecognizer.delegate = self;
    tapGestureRecognizer.numberOfTapsRequired = 2;
    [tapGestureRecognizer addTarget:self action:@selector(showPopupMenu)];
    [self addGestureRecognizer:tapGestureRecognizer];
    
    // Long press gesture recognizing.
    UILongPressGestureRecognizer *longPressGestureRecognizer = [[UILongPressGestureRecognizer alloc] init];
    longPressGestureRecognizer.delegate = self;
    [longPressGestureRecognizer addTarget:self action:@selector(handleLongPress:)];
    [self.containerView addGestureRecognizer:longPressGestureRecognizer];
}


// Handles container view's touch down.
- (void)handleTouchDown:(TouchDownGestureRecognizer *)gestureRecognizer {
    void (^magnifyBlock)(void) = ^{
        self.containerView.layer.transform = CATransform3DMakeScale(1.5, 1.5, 0.0);
    };
    
    void (^completionBlock)(BOOL) = ^(BOOL finished) {
        void (^restoreBlock)(void) = ^{
            self.containerView.layer.transform = CATransform3DIdentity;
        };
        
        [UIView animateWithDuration:0.1
                              delay:0.0
                            options:UIViewAnimationOptionAllowUserInteraction
                         animations:restoreBlock
                         completion:nil];
    };
    
    [UIView animateWithDuration:0.1
                          delay:0.0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:magnifyBlock
                     completion:completionBlock];
}


// Handles view's long press.
- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        if (!self.removeMode) {
            [self showInRemoveMode:YES activatedByUser:YES];
        }
    }
}


// Calculates the appropiate friction coordinates using a given velocity.
- (void)calculateFrictionForVelocity:(CGPoint)velocity {
    NSInteger friction = [Configuration sharedInstance].friction; // (px/ms)
    NSInteger xSignus = 0;
    NSInteger ySignus = 0;
    
    CGFloat xVelocityAbs = ABS(velocity.x);
    CGFloat yVelocityAbs = ABS(velocity.y);
    
    if (velocity.x != 0.0) {
        xSignus = xVelocityAbs / velocity.x;
    }
    
    if (velocity.y != 0.0) {
        ySignus = yVelocityAbs / velocity.y;
    }
    
    // I use the fact that yFriction = factor * xFriction
    // So I use the absolute velocities to calculate this factor.
    CGFloat factor = yVelocityAbs / xVelocityAbs;
    
    // Pitagoras say that...
    self.xFriction = friction / sqrt(factor * factor + 1);
    self.yFriction = factor * self.xFriction;
    
    // Set appropiate signus.
    self.xFriction *= xSignus;
    self.yFriction *= ySignus;
    
    // Adapt friction to the timer interval.
    CGFloat timerIntervalInSeconds = [Configuration sharedInstance].timerInterval / 1000.0;
    self.xFriction *= timerIntervalInSeconds;
    self.yFriction *= timerIntervalInSeconds;
}


// Applies friction effect to a given translation.
- (CGPoint)slowDownTranslation:(CGPoint)translation {
    CGFloat x = translation.x;
    CGFloat y = translation.y;
    
    if (ABS(x) >= 1.0) {
        x -= self.xFriction;
    } else {
        x = 0;
    }
    
    if (ABS(y) >= 1.0) {
        y -= self.yFriction;
    } else {
        y = 0;
    }
    
    return CGPointMake(x, y);
}


// Builds a rectangle which contains the bound that must be reached by the
// bubble.
- (void)calculateDestinationRect:(CGPoint)destinationPoint {
    CGRect superFrame = self.superview.frame;
    
    if (destinationPoint.x == 0.0) {
        // Left bound.
        self.destinationRect = CGRectMake(0.0,
                                          0.0,
                                          DESTINATION_RECT_SIZE,
                                          superFrame.size.height);
        
    } else if (destinationPoint.y == 0.0) {
        // Top bound.
        self.destinationRect = CGRectMake(0.0,
                                          0.0,
                                          superFrame.size.width,
                                          DESTINATION_RECT_SIZE);
        
    } else if (destinationPoint.x == superFrame.size.width - 1.0) {
        // Right bound.
        self.destinationRect = CGRectMake(superFrame.size.width - DESTINATION_RECT_SIZE,
                                          0.0,
                                          DESTINATION_RECT_SIZE,
                                          superFrame.size.height);
        
    } else if (destinationPoint.y == superFrame.size.height - 1.0) {
        // Bottom bound.
        self.destinationRect = CGRectMake(0.0,
                                          superFrame.size.height - DESTINATION_RECT_SIZE,
                                          superFrame.size.width,
                                          DESTINATION_RECT_SIZE);
    }
}


// Initializes and sets up the bubble's popup menu.
- (void)setUpPopupMenu {
    self.popupMenu = [[QBPopupMenu alloc] init];
    
    QBPopupMenuItem *nameItem = [QBPopupMenuItem itemWithTitle:self.bubble.name target:nil action:nil];
    nameItem.enabled = NO;
    
    PushButton *pushButton = [PushButton pushButton];
    [pushButton addTarget:self action:@selector(pushButtonAction) forControlEvents:UIControlEventTouchUpInside];
    QBPopupMenuItem *buttonItem = [QBPopupMenuItem itemWithCustomView:pushButton target:nil action:nil];
    buttonItem.enabled = NO;
    
    self.popupMenu.items = [NSArray arrayWithObjects:nameItem, buttonItem, nil];
}


// Shows the popup associated with this bubble.
- (void)showPopupMenu {
    if (!self.removeMode) {
        [self.popupMenu showInView:self.superview atPoint:CGPointMake(self.center.x, self.frame.origin.y)];
    }
}


// We stop the animations being performed in the view, and set its center
// in the point in which the view was while animating.
- (void)stopTranslationAnimation {
    CALayer *presentationLayer = self.layer.presentationLayer;
    CGPoint position = presentationLayer.position;
    [self.layer removeAllAnimations];
    self.center = position;
}


// This method is called when the push button of the popover menu is tapped.
- (void)pushButtonAction {
    self.onPushButtonTapBlock();
}


# pragma mark - IBAction

- (IBAction)removeButtonAction:(id)sender {
    [self kill];
}

@end
