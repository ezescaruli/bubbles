//
//  ShadowedImageContainerViewController.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/29/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ShadowedImageContainerViewController.h"
#import "UIView+Frame.h"

#define SHADOW_MARGIN       2.0
#define SHADOW_CURL_FACTOR  8.0
#define SHADOW_DEPTH        2.0

@interface ShadowedImageContainerViewController ()

@property (nonatomic, weak) IBOutlet UIView *imageContainerView;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIView *editContainerView;
@property (nonatomic, weak) IBOutlet UILabel *editLabel;

- (void)handleImageSingleTap;
- (void)handleEditSingleTap;

@end

@implementation ShadowedImageContainerViewController


- (NSString *)nibName {
    return NSStringFromClass([ShadowedImageContainerViewController class]);
}


- (id)initWithImage:(UIImage *)image {
    self = [super init];
    
    if (self != nil) {
        self.image = image;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imageView.image = self.image;
    
    // Shadow.
    CGSize size = self.view.bounds.size;
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(0.0, 0.0)];
    
    [path addLineToPoint:CGPointMake(size.width - SHADOW_MARGIN, 0.0)];
    [path addLineToPoint:CGPointMake(size.width - SHADOW_MARGIN, size.height + SHADOW_DEPTH)];
    
    [path addCurveToPoint:CGPointMake(SHADOW_MARGIN, size.height + SHADOW_DEPTH)
            controlPoint1:CGPointMake(size.width - SHADOW_MARGIN - SHADOW_CURL_FACTOR,
                                      size.height + SHADOW_DEPTH - SHADOW_CURL_FACTOR)
            controlPoint2:CGPointMake(SHADOW_MARGIN + SHADOW_CURL_FACTOR,
                                      size.height + SHADOW_DEPTH - SHADOW_CURL_FACTOR)];
    
    self.view.layer.shadowColor = [UIColor blackColor].CGColor;
    self.view.layer.shadowOpacity = 0.5;
    self.view.layer.shadowOffset = CGSizeMake(0.0, 5.0);
    self.view.layer.shadowRadius = 1.0;
    self.view.layer.masksToBounds = NO;
    self.view.layer.shadowPath = path.CGPath;
    
    // Edit setup.
    self.editContainerView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.25];
    self.editLabel.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5];
    
    // Single tap on image container view.
    UITapGestureRecognizer *imageTapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    [imageTapGestureRecognizer addTarget:self action:@selector(handleImageSingleTap)];
    [self.imageContainerView addGestureRecognizer:imageTapGestureRecognizer];
    
    // Single tap on editing container view.
    UITapGestureRecognizer *editTapGestureRecognizer = [[UITapGestureRecognizer alloc] init];
    [editTapGestureRecognizer addTarget:self action:@selector(handleEditSingleTap)];
    [self.editContainerView addGestureRecognizer:editTapGestureRecognizer];
}


- (void)setImage:(UIImage *)anImage {
    if (_image != anImage) {
        _image = anImage;
        self.imageView.image = _image;
    }
}


- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    
    void (^animationBlock)(void) = ^{
        self.editContainerView.alpha = editing ? 1.0 : 0.0;
    };
    
    [UIView animateWithDuration:0.25 animations:animationBlock];
}


# pragma mark - Private methods

// Method called when the image container view is tapped.
- (void)handleImageSingleTap {
    self.onTapBlock();
}


// Shows a list of editing options to choose and change the image.
- (void)handleEditSingleTap {
    self.onEditTapBlock();
}


@end
