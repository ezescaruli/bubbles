//
//  Configuration.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/28/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "Configuration.h"

static Configuration *instance = nil;

@implementation Configuration

+ (Configuration *)sharedInstance {
    if (instance == nil) {
        instance = [[Configuration alloc] init];
    }
    
    return instance;
}

- (id)init {
    self = [super init];
    
    if (self != nil) {
        self.panningMethod = PanningMethodAnimation;
        self.timerInterval = 5;
        self.friction = 5;
    }
    
    return self;
}

- (NSArray *)panningMethods {
    return @[[NSNumber numberWithInteger:PanningMethodAnimation],
             [NSNumber numberWithInteger:PanningMethodTimer]];
}

@end
