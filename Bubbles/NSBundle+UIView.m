//
//  NSBundle+UIView.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/29/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "NSBundle+UIView.h"

@implementation NSBundle (UIView)

- (UIView *)firstViewFromNibNamed:(NSString *)nibName {
    NSArray *xib = [[NSBundle mainBundle] loadNibNamed:nibName
                                                 owner:nil
                                               options:nil];
    
    return [xib objectAtIndex:0];
}

@end
