//
//  ConfigurationDataSource.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/28/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "ConfigurationTableDataSource.h"
#import "Configuration.h"
#import "Constants.h"

@interface ConfigurationTableDataSource()

@property (nonatomic, strong) NSMutableArray *sectionTitles;
@property (nonatomic, strong) NSArray *panningMethodsTitles;
@property (nonatomic, strong) NSArray *timerParametersTitles;

- (UITextField *)textFieldForCell;

@end

@implementation ConfigurationTableDataSource

- (id)init {
    self = [super init];
    
    if (self != nil) {
        self.sectionTitles = [NSMutableArray arrayWithObjects:@"Panning method", nil];
        self.panningMethodsTitles = @[@"Animation", @"Timer"];
        self.timerParametersTitles = @[@"Timer interval (ms)", @"Friction (px/ms)"];
    }
    
    return self;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    if (indexPath.section == 0) {
        // Panning method section.
        PanningMethod panningMethod = indexPath.row;
        cell.textLabel.text = [self.panningMethodsTitles objectAtIndex:panningMethod];
        
        if ([Configuration sharedInstance].panningMethod == panningMethod) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        } else {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
    
    if (indexPath.section == 1) {
        if ([Configuration sharedInstance].panningMethod == PanningMethodTimer) {
            // Timer parameters section.
            cell.textLabel.text = [self.timerParametersTitles objectAtIndex:indexPath.row];
            cell.accessoryType = UITableViewCellAccessoryNone;
            
            if (indexPath.row == 0) {
                // Timer interval row.
                self.timerIntervalTextField = [self textFieldForCell];
                self.timerIntervalTextField.delegate = self;
                [cell.contentView addSubview:self.timerIntervalTextField];
                
                NSInteger timerInterval = [Configuration sharedInstance].timerInterval;
                self.timerIntervalTextField.text = [NSString stringWithFormat:@"%d", timerInterval];
            }
            
            if (indexPath.row == 1) {
                // Friction interval row.
                self.frictionTextField = [self textFieldForCell];
                self.frictionTextField.delegate = self;
                [cell.contentView addSubview:self.frictionTextField];
                
                NSInteger friction = [Configuration sharedInstance].friction;
                self.frictionTextField.text = [NSString stringWithFormat:@"%d", friction];
            }
        }
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return [[Configuration sharedInstance] panningMethods].count;
    }
    
    if (section == 1) {
        if ([Configuration sharedInstance].panningMethod == PanningMethodTimer) {
            return 2;
        }
    }
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSString *frictionTitle = @"Friction parameters";
    
    if ([Configuration sharedInstance].panningMethod != PanningMethodTimer) {
        [self.sectionTitles removeObject:frictionTitle];
    } else {
        if (![self.sectionTitles containsObject:frictionTitle]) {
            [self.sectionTitles insertObject:frictionTitle atIndex:1];
        }
    }
    
    return self.sectionTitles.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.sectionTitles objectAtIndex:section];
}

# pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.timerIntervalTextField) {
        NSInteger interval = [self.timerIntervalTextField.text intValue];
        
        if (interval <= 1000) {
            [Configuration sharedInstance].timerInterval = interval;
        }
    }
    
    if (textField == self.frictionTextField) {
        NSInteger friction = [self.frictionTextField.text intValue];
        [Configuration sharedInstance].friction = friction;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

# pragma mark - Private methods

// Creates a text field which dimensions fit in a table cell.
- (UITextField *)textFieldForCell {
    UITextField *ret = [[UITextField alloc] init];
    ret.frame = kDefaultCellTextFieldRect;
    ret.adjustsFontSizeToFitWidth = YES;
    ret.textColor = kAppleLightBlueColor;
    ret.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    ret.returnKeyType = UIReturnKeyDone;
    ret.backgroundColor = [UIColor clearColor];
    ret.textAlignment = NSTextAlignmentRight;
    
    return ret;
}


@end
