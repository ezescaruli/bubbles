//
//  UIImage+Storage.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/9/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "UIImage+Storage.h"

@implementation UIImage (Storage)

+ (UIImage *)imageFromCacheWithName:(NSString *)name {
    NSString *path = [UIImage completePathForImageNamed:name];
    return [UIImage imageWithContentsOfFile:path];
}

+ (void)deleteFromCacheImageNamed:(NSString *)name {
    NSString *path = [UIImage completePathForImageNamed:name];
    
    // We delete the image in a separate thread in order to avoid loss of
    // performance on main thread.
    [NSThread detachNewThreadSelector:@selector(selectorForRemoveFromCacheWithPath:)
                             toTarget:self
                           withObject:path];
}

+ (void)selectorForRemoveFromCacheWithPath:(NSString *)path {
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    [fileManager removeItemAtPath:path error:&error];
}

- (void)saveInCacheWithName:(NSString *)name {
    NSString *path = [UIImage completePathForImageNamed:name];
    
    // We save the image in a separate thread in order to avoid loss of
    // performance on main thread.
    [NSThread detachNewThreadSelector:@selector(selectorForSaveInCacheWithPath:)
                             toTarget:self
                           withObject:path];
}

- (void)selectorForSaveInCacheWithPath:(NSString *)path {
    NSData *imageData = UIImageJPEGRepresentation(self, 1.0);
    [imageData writeToFile:path atomically:YES];
}


# pragma mark - Private methods


+ (NSString *)completePathForImageNamed:(NSString *)name {
    NSArray *directoryPath = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [directoryPath objectAtIndex:0];
    NSString *path = [cachesDirectory stringByAppendingPathComponent:name];
    
    return [path stringByAppendingPathExtension:@"jpg"];
}


@end
