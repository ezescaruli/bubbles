//
//  TouchDownGestureRecognizer.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/8/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchDownGestureRecognizer : UIGestureRecognizer

@end
