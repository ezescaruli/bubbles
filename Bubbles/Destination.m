//
//  Destination.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/26/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "Destination.h"

@implementation Destination

- (id)initWithPoint:(CGPoint)point time:(CGFloat)time {
    self = [super init];
    
    if (self != nil) {
        self.point = point;
        self.time = time;
    }
    
    return self;
}

@end
