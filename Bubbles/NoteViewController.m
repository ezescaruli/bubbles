//
//  NoteViewController.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/14/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "NoteViewController.h"
#import "DALinedTextView.h"
#import "CoreDataManager.h"
#import "Bubble.h"

@interface NoteViewController ()

@property (nonatomic, weak) IBOutlet DALinedTextView *textView;

- (void)doneAction;

@end

@implementation NoteViewController


- (id)initWithNote:(Note *)note {
    self = [super init];
    
    if (self != nil) {
        self.note = note;
    }
    
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Controller's title.
    NSString *title;
    
    if (self.note.text == nil || self.note.text.length == 0) {
        title = @"New note";
    } else {
        title = [NSString stringWithFormat:@"Note for %@", self.note.bubble.name];
    }
    
    self.title = title;
    
    // Controller's navigation right button, if it's modal.
    
    if (self.presentingViewController != nil) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:self
                                                                             action:@selector(doneAction)];
    }
    
    self.textView.text = self.note.text;
    
    if (self.textView.text == nil || self.textView.text.length == 0) {
        [self.textView becomeFirstResponder];
    }
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Here, we apply the changes to the note model object.
    self.note.text = self.textView.text;
    [[CoreDataManager sharedInstance] saveChanges];
}


# pragma mark - Private methods


- (void)doneAction {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
