//
//  NSBundle+UIView.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/29/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (UIView)

- (UIView *)firstViewFromNibNamed:(NSString *)nibName;

@end
