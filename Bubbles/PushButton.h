//
//  PushButton.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 6/23/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PushButton : UIButton

+ (PushButton *)pushButton;

@end
