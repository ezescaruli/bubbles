//
//  Bubble.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/7/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "Note.h"


@interface Bubble : NSManagedObject

@property (nonatomic, retain) NSDate *creationDate;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *imagePath;
@property (nonatomic, retain) NSNumber *x;
@property (nonatomic, retain) NSNumber *y;
@property (nonatomic, retain) Note *note;

// Creates a new bubble and its entity in Core Data context and persists it.
+ (Bubble *)bubble;

// Fetches all bubbles from Core Data storage.
+ (NSArray *)allBubbles;

// Removes all bubbles from Core Data storage.
+ (void)deleteAllBubbles;

// Deletes this bubble from Core Data storage.
- (void)deleteBubble;

// Returns a unique identifier for this bubble.
- (NSString *)bubbleId;

@end
