//
//  ConfigurationDataSource.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 5/28/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigurationTableDataSource : NSObject<UITableViewDataSource, UITextFieldDelegate>

@property (nonatomic, strong) UITextField *timerIntervalTextField;
@property (nonatomic, strong) UITextField *frictionTextField;

@end
