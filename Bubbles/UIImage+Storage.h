//
//  UIImage+Storage.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/9/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Storage)

// Loads a PNG image from the cache directory using the name sent
// as a parameter.
+ (UIImage *)imageFromCacheWithName:(NSString *)name;

// Deletes an image from the cache directory.
+ (void)deleteFromCacheImageNamed:(NSString *)name;

// Saves the image within the cache directory, as a PNG, using the name sent
// as a parameter.
- (void)saveInCacheWithName:(NSString *)name;

@end
