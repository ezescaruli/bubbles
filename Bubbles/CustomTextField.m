//
//  CustomTextField.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/9/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "CustomTextField.h"

@interface CustomTextField()

- (CGRect)insetsForEditingModeUsingBounds:(CGRect)bounds;

@end

@implementation CustomTextField

- (id)init {
    self = [super init];
    
    if (self != nil) {
        [self setStyle];
    }
    
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setStyle];
}

- (void)setStyle{
    self.borderStyle = UITextBorderStyleLine;
    self.layer.cornerRadius = 3.0f;
    self.layer.borderColor = [UIColor grayColor].CGColor;
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderWidth = 1.0f;
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 5, 0);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self insetsForEditingModeUsingBounds:bounds];
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    return CGRectInset(bounds, 5, 0);
}


# pragma mark - Private methods

- (CGRect)insetsForEditingModeUsingBounds:(CGRect)bounds {
    // We use a stretched rectangle to keep place for clear button.
    CGRect stretchedBounds = CGRectMake(0,
                                        0,
                                        bounds.size.width - 20,
                                        bounds.size.height);
    
    return CGRectInset(stretchedBounds, 5, 0);
}


@end
