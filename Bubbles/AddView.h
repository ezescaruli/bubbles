//
//  AddView.h
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/13/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddView : UIView

@property (nonatomic, strong) UIImage *leftImage;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) void (^buttonActionBlock)(void);

@end
