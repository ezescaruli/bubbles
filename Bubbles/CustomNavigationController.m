//
//  CustomNavigationController.m
//  Bubbles
//
//  Created by Ezequiel Scaruli on 7/20/13.
//  Copyright (c) 2013 Ezequiel Scaruli. All rights reserved.
//

#import "CustomNavigationController.h"

@implementation CustomNavigationController

- (NSUInteger)supportedInterfaceOrientations {
    UIViewController *topViewController = self.topViewController;
    
    if (topViewController == nil) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
    
    return [topViewController supportedInterfaceOrientations];
}

@end
